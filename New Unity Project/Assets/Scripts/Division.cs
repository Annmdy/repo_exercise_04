﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Division : MonoBehaviour
    {
        public int numberOne;
        public float numberTwo;
        public float sum;


        public void SumOne()
        {
            sum = numberOne / numberTwo;
            Debug.Log(sum);
        }

        public void SumTwo(int numberThree, float numberFour)
        {
            sum = numberThree / numberFour;
            Debug.Log(sum);
        }

        public float SumThree(int numberFive, float numberSix)
        {
            float sumTwo = numberFive / numberSix;
            return sumTwo;
        }

        void Start()
        {
            SumOne();
            SumTwo(3, 4);
            sum = SumThree(5, 6);
            Debug.Log(sum);
        }
    }
}